//Function number 1
function noOfMatchesPlayedPerYear(array) {
  let object = {};
  array.map(function (element) {
    if (!object.hasOwnProperty(element.season)) {
      object[element.season] = 1;
    } else {
      object[element.season]++;
    }
  });

  return object;
}



//Function number 2.
function noOfMatchesWonPerTeam(array) {
  let noOfMatches = {};
  array.map(function (element) {
    if (!noOfMatches.hasOwnProperty(element.season)) {
      noOfMatches[element.season] = {};
    } else {
      if (noOfMatches[element.season].hasOwnProperty(element.winner)) {
        noOfMatches[element.season][element.winner]++;
      } else {
        noOfMatches[element.season][element.winner] = 1;
      }
    }
  });
  return noOfMatches;
}

//Function number 3
function runConceededPerTeam(matches, deliveries) {
  // match_id, batting_team, extra_runs from deliveries
  // match_id from matches
  let matches_2016 = matchesInSpecificSeason(matches, '2016');
  let arrayWithSpecifcId = deliveriesInSpecificSeason(deliveries, matches_2016);

  let matchObject = {};
  arrayWithSpecifcId.map(function (element) {
    if (!matchObject.hasOwnProperty(element.bowling_team)) {
      matchObject[element.bowling_team] = parseInt(element.extra_runs);
    } else {
      matchObject[element.bowling_team] =
        parseInt(matchObject[element.bowling_team]) + parseInt(element.extra_runs);
    }
  });
  return matchObject;
}


//Function 4
// Formula:- total runs /(total balls/6)
// count_noBall, count_batMan, sumTotalRuns
function topEconomicalBowlers(matches, deliveries) {
  let matches_2015 = matchesInSpecificSeason(matches, '2015');
  let arrayWithSpecifcId = deliveriesInSpecificSeason(deliveries, matches_2015);

  let economicalBowlers = {};
  arrayWithSpecifcId.map(function (element) {
    if (!economicalBowlers.hasOwnProperty(element.bowler)) {
      economicalBowlers[element.bowler] = 1;
    } else {
      economicalBowlers[element.bowler] =
        parseInt(economicalBowlers[element.bowler]) + 1;
    }
  });

  let result = [];
  Object.keys(economicalBowlers).map(function (key) {
    let count_noBall = 0;
    let totalRuns = 0;
    let object1 = {};
    arrayWithSpecifcId.map(function (element) {
      if (key === element.bowler) {
        if (parseInt(element.noball_runs) > 0 ||
          parseInt(element.wide_runs) > 0) {
          count_noBall++;
        }
        totalRuns += parseInt(element.total_runs);
      }
    });
    object1[key] = parseFloat((totalRuns / ((parseInt(economicalBowlers[key]) - count_noBall) / 6)).toFixed(2));
    result.push(object1);
  });


  const sortable = Object.fromEntries(result.map(x => Object.entries(x)).flat().sort((a, b) => a[1] - b[1]))
  let topTenEconomicBowlers = Object.fromEntries(Object.entries(sortable).slice(0, 9));
  return topTenEconomicBowlers;
}

//Function to get matches in a particular season
function matchesInSpecificSeason(matches, season) {
  let matches_season = [];
  matches.map(function (element) {
    if (element.season === season) {
      matches_season.push(element);
    }
  });
  return matches_season;
}

//Function to get deliveries in a particular season
function deliveriesInSpecificSeason(deliveries, matches_season) {
  let arrayWithSpecifcId = [];
  matches_season.map(function (element) {
    deliveries.map(function (element2) {
      if (element.id === element2.match_id) {
        arrayWithSpecifcId.push(element2);
      }
    });
  });
  return arrayWithSpecifcId;
}



module.exports = {
  noOfMatchesPlayedPerYear, noOfMatchesWonPerTeam,
  runConceededPerTeam, topEconomicalBowlers
}