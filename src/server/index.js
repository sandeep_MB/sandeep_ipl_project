const csv = require('csv-parser');
const fs = require('fs');
const iplFunction = require('./ipl.js');
const file1 = '.././data/matches.csv';
const file2 = '.././data/deliveries.csv';

function arrayObject(filepath1, filepath2) {
  let array = [];
  fs.createReadStream(filepath1)
    .on('error', () => {
      console.log('There is some error. Please check and try again');
    })

    .pipe(csv())
    .on('data', (row) => {
      array.push(row);
    })
    .on('end', () => {
      let array2 = [];
      fs.createReadStream(filepath2)
        .on('error', () => {
          console.log('There is some error. Please check and try again');
        })

        .pipe(csv())
        .on('data', (row) => {
          array2.push(row);
        })
        .on('end', () => {

          let data_1 = JSON.stringify(iplFunction.noOfMatchesPlayedPerYear(array));
          fs.writeFile('.././public/output/matchesPerYear.json', data_1, (err) => {
            if (err) {
              throw err;
            }
          });
          let data_2 = JSON.stringify(iplFunction.noOfMatchesWonPerTeam(array));
          fs.writeFile('.././public/output/matchesWonPerYear.json', data_2, (err) => {
            if (err) {
              throw err;
            }
          });
          let data_3 = JSON.stringify(iplFunction.runConceededPerTeam(array, array2));
          fs.writeFile('.././public/output/runsPerTeam.json', data_3, (err) => {
            if (err) {
              throw err;
            }
          });
          let data_4 = JSON.stringify(iplFunction.topEconomicalBowlers(array, array2));
          fs.writeFile('.././public/output/topEconomicalBowlers.json', data_4, (err) => {
            if (err) {
              throw err;
            }
          });
        })
    })
}

arrayObject(file1, file2);

